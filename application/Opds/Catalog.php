<?php
/**
 * Copyright 2013 Paolo Casarini
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once 'Opds/EntryContent.php';

/**
 * Class to describe the content of an OPDS entry.
 * 
 * @author    Paolo Casarini <paolo@casarini.org>
 * @copyright Copyright (c) Paolo Casarini
 * @version   $Id$
 * @package   Opds
 */
class Opds_Catalog implements ArrayAccess{
    /**
     * MIME type for links that goes to another catalog file.
     * 
     * @var string
     */
    const OPDS_ATOM = "application/atom+xml";
    
    /**
     * MIME type for links that points to a valid ePub book
     * 
     * @var string
     */
    const OPDS_EPUP = "application/epub+zip";
    
    const OPDS_PREFIX = 'opds';
    const DC_PREFIX = 'dc';
    
    /**
     * @var array
     */
    private static $_namespaces = array(
        self::DC_PREFIX   => 'http://purl.org/dc/elements/1.1/',
        self::OPDS_PREFIX => 'http://opds-spec.org/2010/catalog'
    );
    
    /**
     * @var array
     */
    private static $_mimetypes = array(
        'png' => 'image/png',
        'gif' => 'image/gif',
        'jpg' => 'image/jpeg'
    );
    
    /**
     * @var DOMElement
     */
    private $_element;
    
    /**
     * A domain o subdomain name used to build identifiers
     *  
     * @var string
     */
    private $_domain;
    
    /**
     * Namespaces to the document element before cml serialization
     * 
     * @var array
     */
    private $_namespacesToAdd = array();
        
    /**
     * Constructor. The $data array must conform to the following format:
     * <code>
     *  array(
     *  'title'       => 'title of the feed', //required
     *  'link'        => 'canonical url to the feed', //required
     *  'lastUpdate'  => 'timestamp of the update date', // optional
     *  'published'   => 'timestamp of the publication date', //optional
     *  'charset'     => 'charset', // required
     *  'description' => 'short description of the feed', //optional
     *  'author'      => 'author/publisher of the feed', //optional
     *  'email'       => 'email of the author', //optional
     *  'copyright'   => 'copyright notice', //optional
     *  'image'       => 'url to image', //optional
     *  'generator'   => 'generator', // optional
     *  'language'    => 'language the feed is written in', // optional
     *  'entries'     => array(
     *                   array(
     *                    'title'        => 'title of the feed entry', //required
     *                    'link'         => 'url to a feed entry', //required
     *                    'description'  => 'short version of a feed entry', // only text, no html, required
     *                    'content'      => 'long version', // can contain html, optional
     *                    'lastUpdate'   => 'timestamp of the publication date', // optional
     *                    'language'     => 'language the entry is written in', // optional
     *                    'author'       => 'author of the entry' //optional
     *                    'cover'        => 'url to the cover image', // optional
     *                    'thumbnail'    => 'url to the cover image', //optional
     *                   ),
     *                   array(
     *                   //data for the second entry and so on
     *                   )
     *                 )
     * );
     * </code>
     *
     * @param  array $data
     */
    public function __construct(array $header, array $entries) {
        $this->_domain = isset($header['domain']) ? $header['domain'] : null; 
        $this->_element = new DOMDocument('1.0', $header['charset']);
        $root = $this->_mapHeaders($header);
        $this->_mapEntries($root, $entries);
        $this->_element = $root;
    }
    
    /**
     * Required by the ArrayAccess interface.
     *
     * @param  string $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        if (strpos($offset, ':') !== false) {
            list($ns, $attr) = explode(':', $offset, 2);
            return $this->_element->hasAttributeNS(self::lookupNamespace($ns), $attr);
        } else {
            return $this->_element->hasAttribute($offset);
        }
    }


    /**
     * Required by the ArrayAccess interface.
     *
     * @param  string $offset
     * @return string
     */
    public function offsetGet($offset)
    {
        if (strpos($offset, ':') !== false) {
            list($ns, $attr) = explode(':', $offset, 2);
            return $this->_element->getAttributeNS(self::lookupNamespace($ns), $attr);
        } else {
            return $this->_element->getAttribute($offset);
        }
    }


    /**
     * Required by the ArrayAccess interface.
     *
     * @param  string $offset
     * @param  string $value
     * @return string
     */
    public function offsetSet($offset, $value)
    {
        $this->ensureAppended();

        if (strpos($offset, ':') !== false) {
            list($ns, $attr) = explode(':', $offset, 2);
            return $this->_element->setAttributeNS(self::lookupNamespace($ns), $attr, $value);
        } else {
            return $this->_element->setAttribute($offset, $value);
        }
    }


    /**
     * Required by the ArrayAccess interface.
     *
     * @param  string $offset
     * @return boolean
     */
    public function offsetUnset($offset)
    {
        if (strpos($offset, ':') !== false) {
            list($ns, $attr) = explode(':', $offset, 2);
            return $this->_element->removeAttributeNS(self::lookupNamespace($ns), $attr);
        } else {
            return $this->_element->removeAttribute($offset);
        }
    }

    /**
     * Generate the header of the feed when working in write mode
     *
     * @param array $array the data to use
     * @return DOMElement the root node of the feed document
     */
	protected function _mapHeaders($array) {
	    $mandatories = array('title', 'link', 'charset');
        foreach ($mandatories as $mandatory) {
            if (!isset($array[$mandatory])) {
                require_once 'Opds/Exception.php';
                throw new Opds_Exception("$mandatory key is missing");
            }
        }
        $feed = $this->_element->createElement('feed');
        $feed->setAttribute('xmlns', 'http://www.w3.org/2005/Atom');

        $title = $this->_element->createElement('title');
        $title->appendChild($this->_element->createCDATASection($array['title']));
        $feed->appendChild($title);

        if (isset($array['author'])) {
            $author = $this->_element->createElement('author');
            $name = $this->_element->createElement('name', $array['author']);
            $author->appendChild($name);
            if (isset($array['email'])) {
                $email = $this->_element->createElement('email', $array['email']);
                $author->appendChild($email);
            }
            if (isset($array['uri'])) {
                $uri = $this->_element->createElement('uri', $array['uri']);
                $author->appendChild($uri);
            }
            $feed->appendChild($author);
        }

        $updated = isset($array['lastUpdate']) ? $array['lastUpdate'] : time();
        $updated = $this->_element->createElement('updated', date(DATE_ATOM, $updated));
        $feed->appendChild($updated);

        if (isset($array['published'])) {
            $published = $this->_element->createElement('published', date(DATE_ATOM, $array['published']));
            $feed->appendChild($published);
        }
        
        $id = isset($array['id']) ? $array['id'] : $this->_buildTagId(
            isset($array['published']) ? $array['published'] : $updated, $array['link']);
        $id = $this->_element->createElement('id', $id);
        $feed->appendChild($id);
        
        $link = $this->_element->createElement('link');
        $link->setAttribute('rel', 'self');
        $link->setAttribute('type', self::OPDS_ATOM);
        $link->setAttribute('href', $array['link']);
        if (isset($array['language'])) {
            $link->setAttribute('hreflang', $array['language']);
        }
        $feed->appendChild($link);

        if (isset($array['subtitle'])) {
            $subtitle = $this->_element->createElement('subtitle');
            $subtitle->appendChild($this->_element->createCDATASection($array['subtitle']));
            $feed->appendChild($subtitle);
        }

        if (isset($array['copyright'])) {
            $copyright = $this->_element->createElement('rights', $array['copyright']);
            $feed->appendChild($copyright);
        }

        if (isset($array['image'])) {
            $image = $this->_element->createElement('logo', $array['image']);
            $feed->appendChild($image);
        }

        $generator = !empty($array['generator']) ? $array['generator'] : 'phpOpds';
        $generator = $this->_element->createElement('generator', $generator);
        $feed->appendChild($generator);

        return $feed;
    }
    
    /**
     * Generate the entries of the feed when working in write mode
     *
     * The following nodes are constructed for each feed entry
     * <entry>
     *    <id>url to feed entry</id>
     *    <title>entry title</title>
     *    <updated>last update</updated>
     *    <link rel="alternate" href="url to feed entry" />
     *    <summary>short text</summary>
     *    <content>long version, can contain html</content>
     * </entry>
     *
     * @param  array      $array the data to use
     * @param  DOMElement $root  the root node to use
     * @return void
     */
    protected function _mapEntries(DOMElement $root, $entries) {
        
        foreach ($entries as $dataentry) {
            $entry = $this->_element->createElement('entry');
            
            $title = $this->_element->createElement('title');
            $title->appendChild($this->_element->createCDATASection($dataentry['title']));
            $entry->appendChild($title);
            
            $updated = isset($dataentry['lastUpdate']) ? $dataentry['lastUpdate'] : time();
            $updated = $this->_element->createElement('updated', date(DATE_ATOM, $updated));
            $entry->appendChild($updated);
            
            
            $id = isset($dataentry['id']) ? $dataentry['id'] : $this->_buildTagId(
                isset($dataentry['published']) ? $dataentry['published'] : $updated, $dataentry['link']);
            $id = $this->_element->createElement('id', $id);
            $entry->appendChild($id);
            
            if (isset($dataentry['content'])) {
                $content = $this->_element->createElement('content');
                if ($dataentry['content'] instanceof Opds_EntryContent) {
                    $content->setAttribute('type', $dataentry['content']->getType());
                    if ($dataentry['content']->getType() == 'xhtml') {
                        $content->appendChild($this->_element->createTextNode($dataentry['content']->getValue()));
                    } else {
                        $content->appendChild($this->_element->createCDATASection($dataentry['content']->getValue()));
                    }
                } else {
                    $content->setAttribute('type', 'html');
                    $content->appendChild($this->_element->createCDATASection($dataentry['content']));
                }
                $entry->appendChild($content);
            }            
            
            switch ($dataentry['type']) {
                case self::OPDS_ATOM:
                    $link = $this->_element->createElement('link');
                    $link->setAttribute('type', self::OPDS_ATOM);
                    $link->setAttribute('href', $dataentry['link']);
                    if (isset($dataentry['language'])) {
                        $link->setAttribute('hreflang', $dataentry['language']);
                    }
                    $entry->appendChild($link);
                    break;
                case self::OPDS_EPUP:
                    $link = $this->_element->createElement('link');
                    $link->setAttribute('rel', "http://opds-spec.org/acquisition/");
                    $link->setAttribute('type', self::OPDS_EPUP);
                    $link->setAttribute('href', $dataentry['link']);
                    if (isset($dataentry['language'])) {
                        $link->setAttribute('hreflang', $dataentry['language']);
                    }
                    $entry->appendChild($link);
                    
                    if (isset($dataentry['author'])) {
                        $author = $this->_element->createElement('author');
                        $name = $this->_element->createElement('name', $dataentry['author']);
                        $author->appendChild($name);
                        $entry->appendChild($author);
                    }
                    
                    if (isset($dataentry['cover'])) {
                        $link = $this->_element->createElement('link');
                        $link->setAttribute('rel', 'http://opds-spec.org/cover');
                        if (($type = self::lookupImageType(substr(strrchr($dataentry['cover'], '.'), 1))) != null) {
                            $link->setAttribute('type', $type);
                        }
                        $link->setAttribute('href', $dataentry['cover']);
                        $entry->appendChild($link);                        
                    }
                    
                    if (isset($dataentry['thumbnail'])) {
                        $link = $this->_element->createElement('link');
                        $link->setAttribute('rel', 'http://opds-spec.org/thumbnail');
                        if (($type = self::lookupImageType(substr(strrchr($dataentry['thumbnail'], '.'), 1))) != null) {
                            $link->setAttribute('type', $type);
                        }
                        $link->setAttribute('href', $dataentry['thumbnail']);
                        $entry->appendChild($link);                        
                    }
                    if (isset($dataentry['price'])) {
                        $price = $this->_element->createElementNS(
                            self::$_namespaces[self::OPDS_PREFIX], self::OPDS_PREFIX . ':price');
                        if ($dataentry['price'] instanceof Opds_EntryPrice) {
                            $price->setAttribute('currencycode', $dataentry['price']->getCurrencyCode());
                            $price->appendChild($this->_element->createTextNode($dataentry['price']->getValue()));
                        } else {
                            $price->setAttribute('currencycode', 'USD');
                            $price->appendChild($this->_element->createTextNode($dataentry['price']));
                        }
                        $entry->appendChild($price);
                    }
                    if (isset($dataentry['identifier'])) {
                        $language = $this->_element->createElementNS(
                            self::$_namespaces[self::DC_PREFIX], self::DC_PREFIX . ':identifier', $dataentry['identifier']);
                        $entry->appendChild($language);
                    }
                    if (isset($dataentry['language'])) {
                        $language = $this->_element->createElementNS(
                            self::$_namespaces[self::DC_PREFIX], self::DC_PREFIX . ':language', $dataentry['language']);
                        $entry->appendChild($language);
                    }
                    if (isset($dataentry['issued'])) {
                        $language = $this->_element->createElementNS(
                            self::$_namespaces[self::DC_PREFIX], self::DC_PREFIX . ':issued', $dataentry['issued']);
                        $entry->appendChild($language);
                    }
                                        
                    break;
                default:
                    require_once 'Opds/Exception.php';                    
                    throw new Opds_Exception('Entry type not supported: ' . $dataentry['type']);
                    
            }
            $root->appendChild($entry); 
        }
    }
        
    /**
     * Build the XML document of the feed
     *
     * @return string the XML document of the feed
     */
    public function saveXml()
    {
        // Return a complete document including XML prologue.
        $doc = new DOMDocument($this->_element->ownerDocument->version,
                               $this->_element->ownerDocument->actualEncoding);
        $doc->appendChild($doc->importNode($this->_element, true));
        $doc->formatOutput = true;

        return $doc->saveXML();
    }

    /**
     * Send feed to a http client with the correct header
     *
     * @return void
     * @throws Opds_Exception if headers have already been sent
     */
    public function send()
    {
        if (headers_sent()) {
            /** 
             * @see Opds_Exception
             */
            require_once 'Opds/Exception.php';
            throw new Opds_Exception('Cannot send ATOM because headers have already been sent.');
        }

        header('Content-Type: application/atom+xml; charset=' . $this->_element->ownerDocument->actualEncoding);

        echo $this->saveXML();
    }
    
    
    /**
     * Build a unique tag identifier based on the specified domanin and the
     * given link and publishing date.
     *  
     * @param string $published
     * @param string $link
     * @return string the unique tag identifier based on the specified domanin and the given link and publishing date.
     */
    private function _buildTagId($published, $link) {
        if ($this->_domain === null) {
            throw new Opds_Exception('You may specify a valid domanin in the header to automatically build identifiers.');
        }
        return 'tag:' . $this->_domain . ',' . date('Y-m-d') . ':' . md5($link);
    }

    /**
     * Get the full version of a namespace prefix
     *
     * Looks up a prefix (atom:, etc.) in the list of registered
     * namespaces and returns the full namespace URI if
     * available. Returns the prefix, unmodified, if it's not
     * registered.
     *
     * @return string
     */
    public static function lookupNamespace($prefix)
    {
        return isset(self::$_namespaces[$prefix]) ?
            self::$_namespaces[$prefix] :
            $prefix;
    }
    
    private static function lookupImageType($extension) {
        return isset(self::$_mimetypes[$extension]) ?
            self::$_mimetypes[$extension] :
            null;
    }
}
