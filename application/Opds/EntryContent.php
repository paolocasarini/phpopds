<?php
/**
 * Copyright 2013 Paolo Casarini
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class to describe the content of an OPDS entry.
 * 
 * @author    Paolo Casarini <paolo@casarini.org>
 * @copyright Copyright (c) Paolo Casarini
 * @version   $Id$
 * @package   Opds
 */
class Opds_EntryContent {
    private $_type;
    private $_value;
    
    /**
     * Constructor
     * 
     * @param string $value the actual content
     * @param string $type the content type (text|html|xhtml)
     */
    public function __construct($value, $type = 'html') {
        $this->_type = $type;
        $this->_value = $value;
    }
    
    /**
     * Returns the content type.
     * 
     * @return string the content type.
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Returns the actual content.
     * 
     * @return string the actual content
     */
    public function getValue() {
        return $this->_value;
    }
}
