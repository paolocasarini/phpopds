<?php
/**
 * Copyright 2013 Paolo Casarini
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class to describe the price of an OPDS entry.
 * 
 * @author    Paolo Casarini <paolo@casarini.org>
 * @copyright Copyright (c) Paolo Casarini
 * @version   $Id$
 * @package   Opds
 */
class Opds_EntryPrice {
    private $_currencyCode;
    private $_value;
    
    /**
     * Constructor
     * 
     * @param string $value the actual price
     * @param string $currencyCode the currency code (MUST be an "Alphabetic code" from [ISO4127])
     */
    public function __construct($value, $currencyCode = 'USD') {
        $this->_currencyCode = $currencyCode;
        $this->_value = $value;
    }
    
    /**
     * Returns the currency code
     * 
     * @return string the currency code.
     */
    public function getCurrencyCode() {
        return $this->_currencyCode;
    }

    /**
     * Returns the actual price.
     * 
     * @return string the actual price
     */
    public function getValue() {
        return $this->_value;
    }
}
