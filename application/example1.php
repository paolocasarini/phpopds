<?php
/**
 * Copyright 2013 Paolo Casarini
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require_once 'Opds/Catalog.php';

/**
 * Example that produce an OPDS feed with two atoms catalogs 
 */
 
$header = array(
    'title' => 'Paolo Casarini\'s Private Book Catalog',
    'subtitle' => 'Categories available',
    'link'  => 'http://www.casarini.org/opds/top.atom',
    'domain' => 'casarini.org',
    'charset' => 'utf8',
    'author' => 'Paolo Casarini',
    'email' => 'paolo@casarini.org',
    'uri' => 'http://www.casarini.org/blog/',
    'copyright' => utf8_encode('Copyright (c) 2010, Paolo Casarini')
);

$entries = array(
    array(
        'title' => 'Agile Software Development',
        'type' => 'application/atom+xml',
        'link' => 'http://www.casarini.org/opds/agile.atom',
        'content' => 'Books that I bought to build my agile experience'
    ),
    array(
        'title' => 'Miscellaneous',
        'type' => 'application/atom+xml',
        'link' => 'http://www.casarini.org/opds/misc.atom',
        'content' => new Opds_EntryContent('My spare time library', 'text')
    )
);

$test = new Opds_Catalog($header, $entries);
print($test->saveXml());
