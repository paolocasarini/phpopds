<?php
/**
 * Copyright 2013 Paolo Casarini
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
require_once 'Opds/Catalog.php';
require_once 'Opds/EntryPrice.php';

/**
 * Example that produce an OPDS feed with two atoms catalogs
 */
$header = array(
    'title' => 'Paolo Casarini\'s Private Book Catalog',
    'subtitle' => 'Agile Software Development',
    'link'  => 'http://www.casarini.org/opds/agile.atom',
    'domain' => 'casarini.org',
    'charset' => 'utf8',
    'author' => 'Paolo Casarini',
    'email' => 'paolo@casarini.org',
    'uri' => 'http://www.casarini.org/blog/',
    'copyright' => utf8_encode('Copyright (c) 2010, Paolo Casarini')
);

$entries = array(
    array(
        'title' => 'the Art of Agile Development',
        'type' => 'application/epub+zip',
        'link' => 'http://www.casarini.org/opds/epubs/agile/TheArtofAgileDevelopment.epub',
        'author' => 'James Shore, Share Warden',
        'content' => new Opds_EntryContent(
            '<div xmlns="http://www.w3.org/1999/xhtml">Plenty of books describe what agile development is or why it helps software projects succeed, but very few combine information for developers, managers, testers, and customers into a single package that they can apply directly. This book provides a gestalt view of the agile development process that serves as a comprehensive introduction for non-technical readers, along with hands-on technical practices for programmers and developers. The book also tackles the people aspect of Extreme Programming.Books that I bought to build my agile experience</div>',
            'xhtml'),
        'cover' => 'http://www.casarini.org/opds/epubs/agile/TheArtofAgileDevelopment.png',
        'thumbnail' => 'http://www.casarini.org/opds/epubs/agile/TheArtofAgileDevelopment_small.jpg',
        'language' => 'en'
    ),
    array(
        'title' => 'Agile Software Development with Scrum',
        'type' => 'application/epub+zip',
        'link' => 'http://www.casarini.org/opds/epubs/agile/ASDwS.epub',
        'author' => 'Ken Schwaber, Mike Beedle',
        'cover' => 'http://www.casarini.org/opds/epubs/agile/cover/ASDwS',
        'price' => new Opds_EntryPrice('35,00', 'EUR'),
        'language' => 'en'
    ),
    array(
        'title' => 'Test-Driven Development',
        'subtitle' => 'By Example',
        'type' => 'application/epub+zip',
        'link' => 'http://www.casarini.org/opds/epubs/agile/TDD_KentBeck.epub',
        'author' => 'Kent Beck',
        'cover' => 'http://www.casarini.org/opds/epubs/agile/cover/TDD_KentBeck',
        'price' => '49.99',
        'identifier' => 'ISBN:0321146530',
        'language' => 'en'
    )

);

$test = new Opds_Catalog($header, $entries);
print($test->saveXml());
