<?php
/**
 * Copyright 2013 Paolo Casarini
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'Opds/EntryPrice.php';

/**
 * EntryPrice test case.
 */
class EntryPriceTest extends PHPUnit_Framework_TestCase {
    
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp() {
        parent::setUp ();
    }
    
    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown() {
        parent::tearDown ();
    }
    
    /**
     * Constructs the test case.
     */
    public function __construct() {
        // TODO Auto-generated constructor
    }
    
    public function testGetCurrencyCodeWithDefault() {
        $entry = new Opds_EntryPrice('1.0');
        $this->assertEquals('USD', $entry->getCurrencyCode());
        $entry = new Opds_EntryPrice('1.0', 'EUR');
        $this->assertNotEquals('USD', $entry->getCurrencyCode());
    }

    public function testGetCurrencyCode() {
        $entry = new Opds_EntryPrice('1.0', 'USD');
        $this->assertEquals('USD', $entry->getCurrencyCode());
        $entry = new Opds_EntryPrice('1.0', 'EUR');
        $this->assertEquals('EUR', $entry->getCurrencyCode());
    }
    
    public function testGetValue() {
        $entry = new Opds_EntryPrice('1.0', 'USD');
        $this->assertEquals('1.0', $entry->getValue());
        $entry = new Opds_EntryPrice(null, 'EUR');
        $this->assertEquals(null, $entry->getValue());
        $entry = new Opds_EntryPrice(3, 'EUR');
        $this->assertEquals(3, $entry->getValue());
    }    
}
